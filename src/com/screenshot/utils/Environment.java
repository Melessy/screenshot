/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.screenshot.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Level;

/**
 *
 * @author T7emon
 */
public class Environment {
    
    public static void copyFile(String srFile, String dtFile) {
        try {
            File f1 = new File(srFile);
            File f2 = new File(dtFile);
            InputStream inz = new FileInputStream(f1);
            OutputStream outz = new FileOutputStream(f2);
            byte[] buf = new byte[1024];
            int len;
            while ((len = inz.read(buf)) > 0) {
                outz.write(buf, 0, len);
            }
            inz.close();
            outz.close();
        } catch (Exception e) {
        }}

    public static void createFiles(File... files) {
        for (File file : files) {
            create(file, false);
        }
    }

    public static void createParentDirectories(File... files) {
        for (File file : files) {
            create(file.getParentFile(), true);
        }
    }

    public static void createDirectories(File... files) {
        for (File file : files) {
            create(file, true);
        }
    }

    public static void create(final File file, final boolean directory) {
        if (directory ? !createDirectory(file) : !createFile(file)) {
                create(file, directory);
                System.out.println("Created " + directory + "> " + file.getAbsolutePath());
                        
        }
    }

    private static void exit() {
        exit(true);
    }

    public static void exit(final boolean notify) {
        System.exit(0);
    }

    private static boolean createDirectory(final File directory) {
        return directory.isDirectory() || directory.mkdirs();
    }

    private static boolean createFile(final File file) {
        if (file.exists()) {
            return true;
        }
        try {
            return file.createNewFile();
        } catch (IOException ex) {
            //Logger.log(Environment.class, Level.WARNING, String.format("Error creating file \'%s\'", file.getAbsolutePath()), ex);
            return false;
        }
    }

    public static void remove(final File file) {
        if (removeFile(file)) {           
                remove(file);
            } else {
                exit();
        }
        createFile(file);
    }

    private static boolean removeFile(final File file) {
        return !file.exists() || file.delete();
    }

}

