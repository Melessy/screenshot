/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.screenshot.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 *
 * @author t7emon
 */
public class Date {
    
           public static String getCurrentDateString()
	    {
	        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	        //DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");
	        return dateFormat.format(new java.util.Date());
	    }
           
             public static String getCurrentTimeString()
	    {
	        //DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
	        DateFormat dateFormat = new SimpleDateFormat("HH-mm-ss");
	        //DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");
	        return dateFormat.format(new java.util.Date());
	    }
    
    
}
