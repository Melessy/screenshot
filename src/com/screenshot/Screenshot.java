package com.screenshot;

import static com.screenshot.Constants.MAIN_DIRECTORY;
import static com.screenshot.Constants.SCREENSHOT_DIRECTORY;
import static com.screenshot.Constants.seperator;
import static com.screenshot.utils.Date.getCurrentDateString;
import static com.screenshot.utils.Date.getCurrentTimeString;
import com.screenshot.utils.Environment;
import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.imageio.ImageIO;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author t7emon
 */
public class Screenshot {
 
    public static void Take() {
        
        if (!Constants.MAIN_DIRECTORY.exists()) {
            Environment.createDirectories(MAIN_DIRECTORY);
            System.out.println("Created --> " + MAIN_DIRECTORY.getAbsolutePath());
        }
        
        if (!Constants.SCREENSHOT_DIRECTORY.exists()) {
            Environment.createDirectories(SCREENSHOT_DIRECTORY);
            System.out.println("Created --> " + SCREENSHOT_DIRECTORY.getAbsolutePath());
        }
        
        try {
            Robot robot = new Robot();
            String format = "png";
            String fileName = "Screenshot"+"_"+getCurrentDateString()+"_"+getCurrentTimeString()+"." + format;
            
            Rectangle screenRect = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
            BufferedImage screenFullImage = robot.createScreenCapture(screenRect);
            ImageIO.write(screenFullImage, format, new File(SCREENSHOT_DIRECTORY + seperator + fileName));
             System.out.println("Saved Screenshot in " + SCREENSHOT_DIRECTORY.getAbsolutePath() + seperator + fileName);
            //System.out.println("A full screenshot saved!");
        } catch (AWTException | IOException ex) {
            System.err.println(ex);
        }
    }
}
