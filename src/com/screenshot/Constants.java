package com.screenshot;

import static com.screenshot.utils.Date.getCurrentDateString;
import java.io.File;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author t7emon
 */
public class Constants {
    
        public static String name = "Javac";
        public static String homeDir = System.getProperty("user.home");
        public static String seperator = System.getProperty("file.separator");
        
            public static final File MAIN_DIRECTORY = new File(homeDir, name);
            public static final File SCREENSHOT_DIRECTORY = new File(MAIN_DIRECTORY, getCurrentDateString());
}
